// console.log("Hello, Full Stack!");

// For selecting HTML elements, we'll be using: document.querySelector
// Syntax: document.querySelector("htmlElement");
	/*
		"document" - refers to the whole page
		".querySelector" - used to select a specific object/HTML element from the document
	*/
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


//[SECTION] Event Listeners
	// Whenever a user interacts with a web page, this action is considered as an event
	// The method used is "addEventListener". It takws 2 arguments:
		//1. A string identifying an event.
		// 2. A function that the listener will execute once the event is triggered
txtFirstName.addEventListener('keyup', (event) => {

	// The "innerHTML" property sets or returns the value of the element
	// The ".value" property sets or returns the value of an attribute. (form control elements: input, select, etc.)

	spanFullName.innerHTML = txtFirstName.value;
})

// Multiple listeners can also be assigned to the same event
txtFirstName.addEventListener("keyup", (event) => {

	// event.target containes the element where the event happened
	console.log(event.target);

	// event.target.value gets the value of the input
	console.log(event.target.value);
});


// Mini-activity
txtLastName.addEventListener("keyup", (event) => {

	spanFullName.innerHTML = txtLastName.value;
});