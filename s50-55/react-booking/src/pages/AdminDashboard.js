import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	// Create allCourses state to contain the courses from the response of our fetch data.
	const [allCourses, setAllCourses] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [courseId, setCourseId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [slots, setSlots] = useState(0);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add course modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	// To control the edit course modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific course and bind it with our input fields.
	const openEdit = (id) => {
		setCourseId(id);

		// Getting a specific course to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/courses/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the course states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setSlots(data.slots);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setSlots(0);

		setShowEdit(false);
	};

	// [SECTION] To view all course in the database (active & inactive)
	// fetchData() function to get all the active/inactive courses.
	const fetchData = () =>{
		// get all the courses from the database
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllCourses(data.map(course => {
				return (
					<tr key={course._id}>
						<td>{course._id}</td>
						<td>{course.name}</td>
						<td>{course.description}</td>
						<td>{course.price}</td>
						<td>{course.slots}</td>
						<td>{course.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the course
								(course.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(course._id, course.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(course._id, course.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(course._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all courses in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])

	// [SECTION] Setting the course to Active/Inactive

	// Making the course inactive
	const archive = (id, courseName) =>{
		console.log(id);
		console.log(courseName);

		// Using the fetch method to set the isActive property of the course document to false
		fetch(`${process.env.REACT_APP_API_URL}/courses/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${courseName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the course active
	const unarchive = (id, courseName) =>{
		console.log(id);
		console.log(courseName);

		// Using the fetch method to set the isActive property of the course document to false
		fetch(`${process.env.REACT_APP_API_URL}/courses/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${courseName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new course
	// Inserting a new course in our database
	const addCourse = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    slots: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	}

	// [SECTION] Edit a specific course
	// Updating a specific course in our database
	// edit a specific course
	const editCourse = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    slots: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	} 

	// Submit button validation for add/edit course
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name != "" && description != "" && price > 0 && slots > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, slots]);

	return(
		(user.isAdmin)
		?
		<>
			{/*Header for the admin dashboard and functionality for create course and show enrollments*/}
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*Adding a new course */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Course</Button>
				{/*To view all the user enrollments*/}
				<Button variant="secondary" className="
				mx-2">Show Enrollments</Button>
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the courses in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Course ID</th>
		          <th>Course Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Slots</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allCourses}
		      </tbody>
		    </Table>
			{/*End of table for course viewing*/}

	    	{/*Modal for Adding a new course*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addCourse(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Course</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Course Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Course Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Course Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Course Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Course Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Course Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="slots" className="mb-3">
	    	                <Form.Label>Course Slots</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Course Slots" 
	    		                value = {slots}
	    		                onChange={e => setSlots(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding course*/}

    	{/*Modal for Editing a course*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editCourse(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Course</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Course Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Course Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Course Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Course Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Course Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Course Slots</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Slots" 
    		                value = {slots}
    		                onChange={e => setSlots(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a course*/}
		</>
		:
		<Navigate to="/courses" />
	)
}
