let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}


function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}


function dequeue() {

  let frontElement = collection[0];
  
  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }
  
  collection.length--;
  
  return collection;
}



function front() {
  return collection[0];
}



function size() {
  return collection.length;
}


function isEmpty() {
	if (collection.length > 0) {
		return false
	} else {
		return true
	}
}




module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};